package com.greenloggia.assessment.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.greenloggia.assessment.entity.Product;
import com.greenloggia.assessment.service.ProductService;

@Controller
public class ProductController {
	@Autowired
	ProductService productService;

	@GetMapping("/viewProducts")
	public String getAllProduct(Model model) {
		List<Product> products = new ArrayList<>();
		products = productService.findAll();
		model.addAttribute("products", products);
		return "product";
	}

	@PostMapping("/addProduct")
	public String addProduct(@ModelAttribute Product product, Model model) {
		String message = productService.addProduct(product);
		model.addAttribute("message", message);
		return "product";
	}

	@PostMapping("updateProduct")
	public String updateProduct(@ModelAttribute Product product, Model model) {
		String message = productService.updateProduct(product);
		model.addAttribute("message", message);
		return "product";
	}

	@GetMapping("/delete/{id}")
	public String deleteProduct(@PathVariable Integer id, Model model) {
		productService.deleteProduct(id);
		model.addAttribute("message", "Delete success!");
		return "product";
	}
}
