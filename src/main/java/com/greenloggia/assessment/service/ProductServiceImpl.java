package com.greenloggia.assessment.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greenloggia.assessment.entity.Product;
import com.greenloggia.assessment.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService{
	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public List<Product> findAll() {
		List<Product> products = productRepository.findAll();
		return products;
	}

	@Override
	public String addProduct(Product product) {
		productRepository.save(product);
		return "success";
	}

	@Override
	public String updateProduct(Product product) {
		Product existProduct = productRepository.findById(product.getId()).orElse(null);
		
		if (existProduct != null) {
			// Merge new value to existed product
			existProduct.setName(product.getName());
			existProduct.setPrice(product.getPrice());
			existProduct.setImg(product.getImg());
			existProduct.setInStock(product.getInStock());
			existProduct.setShortDescription(product.getShortDescription());

			// Save and update
			productRepository.save(existProduct);
			return "updated";
		}
		return "update failed";
	}

	@Override
	public void deleteProduct(Integer id) {
		productRepository.deleteById(id);
	}
	
}
