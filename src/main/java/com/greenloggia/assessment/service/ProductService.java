package com.greenloggia.assessment.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.greenloggia.assessment.entity.Product;

@Service
public interface ProductService {
	public List<Product> findAll();
	public String addProduct(Product product);
	public String updateProduct(Product product);
	public void deleteProduct(Integer id);
}
